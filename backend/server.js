const express = require("express");
const cors = require("cors");
const app = express();
const { default: axios } = require("axios");
const fs = require('fs')
var multer = require('multer');
const FormData = require("form-data");
const pinataSDK = require('@pinata/sdk');
const pinata = new pinataSDK('e5018ddef1ce180b0660', '76ef9089e7889107690cf61cfcc829991c4a6eca92792627a7d0c1fe8967f465');




app.use(cors());

app.get('/', function (req, res) {
    res.render('form');
});

app.set('view engine', 'pug');
app.set('views', './views');

app.use(express.json());

app.use(express.urlencoded({ extended: true }));

// app.use(upload.array()); 
app.use(express.static('public'));


imageUrl = '/home/user/Desktop/nft-marketplace/backend/assets'
const storageEngine = multer.diskStorage({
    destination: imageUrl,
    filename: (req, file, cb) => {
        cb(null, `${file.originalname}`);
    },
});

const upload = multer({
    storage: storageEngine,
});

app.post('/create', upload.single('image'), async (req, res) => {

    pinJSONToIPFS = async (pinataApiKey, pinataSecretApiKey) => {
        const url = 'https://api.pinata.cloud/pinning/pinFileToIPFS';
        const formData = new FormData();

        file = fs.createReadStream(`./assets/${req.file.originalname}`)
        formData.append('file', file);


        //we gather a local file from the API for this example, but you can gather the file from anywhere
        try {
            const response = await axios.post(url,
                formData,
                {
                    headers: {
                        'pinata_api_key': pinataApiKey,
                        'pinata_secret_api_key': pinataSecretApiKey,
                        'Content-Type': `multipart/form-data; boundary=${formData._boundary}`,
                    }
                }
            );

            return response.data.IpfsHash;
        } catch (error) {
            console.log(error); //handle error here
        }
    }
    res.json(await pinJSONToIPFS('e5018ddef1ce180b0660', '76ef9089e7889107690cf61cfcc829991c4a6eca92792627a7d0c1fe8967f465'));



})


app.post('/createNft', async (req, res) => {
    pinata.pinJSONToIPFS(req.body).then((result) => {
        //handle results here
        res.json(result.IpfsHash)
    }).catch((err) => {
        //handle error here
        console.log(err);
    });
})


app.post('/fetchAll', async (req, res) => {
    try {


        const object = []
        for (let i = 0; i < req.body.length; i++) {
            const url = `https://gateway.pinata.cloud/ipfs/${req.body[i].uri}/?filename=tokenURI.json`;
            const fileRes = await axios.get(url, {
                headers: {
                    pinata_api_key: "e5018ddef1ce180b0660",
                    pinata_secret_api_key: "76ef9089e7889107690cf61cfcc829991c4a6eca92792627a7d0c1fe8967f465",
                },
            });
            
            object.push(fileRes.data)
            await (object[i].address = req.body[i].address);
            await (object[i].tokenId = req.body[i].tokenId);
            await (object[i].price = req.body[i].price);
            await (object[i].highestBid = req.body[i].highestBid ?? 0);
            object[i].timestamp = new Date(req.body[i].timestamp * 1000).toLocaleTimeString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true });
        }
        res.json(object)
    } catch (err) {
        console.log(err)
    }
})
const PORT = 8000;
app.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}.`);
})
