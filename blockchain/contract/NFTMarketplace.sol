// SPDX-License-Identifier: SEE LICENSE IN LICENSE
pragma solidity ^0.8.10;

import "@openzeppelin/contracts/utils/Counters.sol";
import "@openzeppelin/contracts/token/ERC721/ERC721.sol";
import "@openzeppelin/contracts/token/ERC721/extensions/ERC721URIStorage.sol";

contract NFTMarketplace is ERC721URIStorage {
    using Counters for Counters.Counter;

    Counters.Counter private _tokenIds;
    Counters.Counter private _itemsSold;

    uint256 listingPrice = 0.00025 ether;

    address payable owner;

    mapping(uint256 => MarketItem) private idToMarketItem;

    mapping(uint256 => address) public highestBidder;

    mapping(uint256 => uint256) public highestBid;

    mapping(uint256 => bool) public started;

    mapping(uint256 => bool) public ended;

    mapping(address => uint256) public bids;

    mapping(uint256 => uint256) public endAt;
    

    struct MarketItem {
        uint256 tokenId;
        address payable seller;
        address payable owner;
        uint256 price;
        bool sold;
        bool auction;
    }

    event MarketItemCreated(
        uint256 indexed tokenId,
        address seller,
        address owner,
        uint256 price,
        bool sold,
        bool auction
    );

    event Start();
    event Bid(address indexed sender, uint256 amount);
    event Withdraw(address indexed bidder, uint256 amount);
    event End(address winner, uint256 amount);

    uint256 public id;


    constructor() ERC721("NFT Tokens", "NTT") {
        owner = payable(msg.sender);
    }

    function updateListingPrice(uint256 _listingPrice) public payable {
        require(
            owner == msg.sender,
            "Only marketplace owner can update listing price."
        );
        listingPrice = _listingPrice;
    }

    function getListingPrice() public view returns (uint256) {
        return listingPrice;
    }

    function createMarketItem(uint256 tokenId, uint256 price) private {
        require(price > 0, "Price must be at least 1 wei");

        require(
            msg.value == listingPrice,
            "Price must be equal to listing price"
        );

        idToMarketItem[tokenId] = MarketItem(
            tokenId,
            payable(msg.sender),
            payable(address(this)),
            price,
            false,
            false
        );

        _transfer(msg.sender, address(this), tokenId);

        emit MarketItemCreated(
            tokenId,
            msg.sender,
            address(this),
            price,
            false,
            false
        );
    }

    function createToken(string memory tokenURI, uint256 price)
        public
        payable
        returns (uint256)
    {
        _tokenIds.increment();

        uint256 newTokenId = _tokenIds.current();

        _mint(msg.sender, newTokenId);
        _setTokenURI(newTokenId, tokenURI);
        createMarketItem(newTokenId, price);

        return newTokenId;
    }

    function resellToken(uint256 tokenId, uint256 price) public payable {
        require(
            idToMarketItem[tokenId].owner == msg.sender &&
                idToMarketItem[tokenId].auction == false,
            "Only item owner can perform this operation"
        );
        require(
            msg.value == listingPrice,
            "Price must be equal to listing price"
        );
        idToMarketItem[tokenId].sold = false;
        idToMarketItem[tokenId].price = price;
        idToMarketItem[tokenId].seller = payable(msg.sender);
        idToMarketItem[tokenId].owner = payable(address(this));
        _itemsSold.decrement();

        _transfer(msg.sender, address(this), tokenId);
    }

    function createMarketSale(uint256 tokenId) public payable {
        uint256 price = idToMarketItem[tokenId].price;
        address payable creator = idToMarketItem[tokenId].seller;
        require(
            msg.value == price,
            "Please submit the asking price in order to complete the purchase"
        );
        idToMarketItem[tokenId].owner = payable(msg.sender);
        idToMarketItem[tokenId].sold = true;
        idToMarketItem[tokenId].seller = payable(address(0));
        _itemsSold.increment();

        _transfer(address(this), msg.sender, tokenId);
        payable(owner).transfer(listingPrice);
        payable(creator).transfer(msg.value);
    }

    function start(uint256 tokenId) external payable {
        require(started[tokenId] == false, "started");
        
        if (msg.sender == idToMarketItem[tokenId].owner) {
            require(
                msg.value == listingPrice,
                "Price must be equal to listing price"
            );

            idToMarketItem[tokenId].sold = false;
            idToMarketItem[tokenId].seller = payable(msg.sender);
            idToMarketItem[tokenId].owner = payable(address(this));
            _itemsSold.decrement();
            _transfer(msg.sender, address(this), tokenId);
        } else {
            require(
                msg.sender == idToMarketItem[tokenId].seller,
                "not an owner"
            );
        }
        started[tokenId] = true;
        ended[tokenId] = false;
        idToMarketItem[tokenId].auction = true;
        endAt[tokenId] = block.timestamp + 300 seconds;
        highestBid[tokenId] = idToMarketItem[tokenId].price;

        emit Start();
    }

    function bid(uint256 tokenId) external payable {
        require(started[tokenId] == true, "not started");
        require(block.timestamp < endAt[tokenId], "ended");
        require(msg.value > highestBid[tokenId], "value < highest");

        if (highestBidder[tokenId] != address(0)) {
            require(bids[msg.sender] == 0, "bidding allowed only once");
            bids[msg.sender] = msg.value;
            
        }

        highestBidder[tokenId] = msg.sender;
        highestBid[tokenId] = msg.value;

        emit Bid(msg.sender, msg.value);
    }

    function withdraw(uint256 tokenId) external {
        require(msg.sender != highestBidder[tokenId]);
        require(bids[msg.sender] != 0 );
        uint256 bal = bids[msg.sender];
        bids[msg.sender] = 0;
        payable(msg.sender).transfer(bal);
        emit Withdraw(msg.sender, bal);
    }

    function end(uint256 tokenId) external {
        require(msg.sender == idToMarketItem[tokenId].seller);
        require(started[tokenId] == true, "not started");
        require(block.timestamp >= endAt[tokenId], "not ended");
        require(ended[tokenId] == false, "ended");

        ended[tokenId] = true;
        started[tokenId] = false;
        if (highestBidder[tokenId] != address(0)) {
            idToMarketItem[tokenId].owner = payable(highestBidder[tokenId]);
            idToMarketItem[tokenId].sold = true;
            idToMarketItem[tokenId].auction = false;
            idToMarketItem[tokenId].seller = payable(address(0));
            idToMarketItem[tokenId].price = highestBid[tokenId];
            _itemsSold.increment();

            _transfer(address(this), highestBidder[tokenId], tokenId);
            payable(owner).transfer(listingPrice);
            (idToMarketItem[tokenId].seller).transfer(highestBid[tokenId]);
        }else{
            idToMarketItem[tokenId].auction = false; 
        }

        emit End(highestBidder[tokenId], highestBid[tokenId]);
    }

    function fetchMarketItems() public view returns (MarketItem[] memory) {
        uint256 itemCount = _tokenIds.current();
        uint256 unsoldItemCount = _tokenIds.current() - _itemsSold.current();
        uint256 currentIndex = 0;

        MarketItem[] memory items = new MarketItem[](unsoldItemCount);
        for (uint256 i = 0; i < itemCount; i++) {
            if (idToMarketItem[i + 1].owner == address(this) && idToMarketItem[i+1].auction == false) {
                uint256 currentId = i + 1;
                MarketItem storage currentItem = idToMarketItem[currentId];
                items[currentIndex] = currentItem;
                currentIndex += 1;
            }
        }
        return items;
    }

    function fetchMyNfts() public view returns (MarketItem[] memory) {
        uint256 totalItemCount = _tokenIds.current();
        uint256 itemCount = 0;
        uint256 currentIndex = 0;

        for (uint256 i = 0; i < totalItemCount; i++) {
            if (idToMarketItem[i + 1].owner == msg.sender) {
                itemCount += 1;
            }
        }

        MarketItem[] memory items = new MarketItem[](itemCount);
        for (uint256 i = 0; i < totalItemCount; i++) {
            if (idToMarketItem[i + 1].owner == msg.sender) {
                uint256 currentId = i + 1;
                MarketItem storage currentItem = idToMarketItem[currentId];
                items[currentIndex] = currentItem;
                currentIndex += 1;
            }
        }

        return items;
    }

    function fetchItemsListed() public view returns (MarketItem[] memory) {
        uint256 totalItemCount = _tokenIds.current();
        uint256 itemCount = 0;
        uint256 currentIndex = 0;

        for (uint256 i = 0; i < totalItemCount; i++) {
            if (idToMarketItem[i + 1].seller == msg.sender) {
                itemCount += 1;
            }
        }

        MarketItem[] memory items = new MarketItem[](itemCount);
        for (uint256 i = 0; i < totalItemCount; i++) {
            if (idToMarketItem[i + 1].seller == msg.sender && idToMarketItem[i+1].auction == false) {
                uint256 currentId = i + 1;
                MarketItem storage currentItem = idToMarketItem[currentId];
                items[currentIndex] = currentItem;
                currentIndex += 1;
            }
        }

        return items;
    }

    function fetchAuction() public view returns (MarketItem[] memory) {
        uint256 totalItemCount = _tokenIds.current();
        uint256 itemCount = 0;
        uint256 currentIndex = 0;

        for (uint256 i = 0; i < totalItemCount; i++) {
            if (idToMarketItem[i + 1].auction == true) {
                itemCount += 1;
            }
        }

        MarketItem[] memory items = new MarketItem[](itemCount);

        for (uint256 i = 0; i < totalItemCount; i++) {
            if (idToMarketItem[i + 1].auction == true) {
                uint256 currentId = i + 1;
                MarketItem storage currentItem = idToMarketItem[currentId];
                items[currentIndex] = currentItem;
                currentIndex += 1;
            }
        }

        return items;
    }
}
