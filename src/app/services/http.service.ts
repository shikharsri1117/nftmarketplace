import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';


const baseUrl = 'http://localhost:8000/';



@Injectable({
  providedIn: 'root'
})
export class HttpService {
  
  constructor(private http:HttpClient) { }

  create(data: any, url:any): Observable<any> {
    return this.http.post(`${baseUrl}${url}`, data,{

    });
  }


  getAll(): Observable<any> {
    return this.http.get(baseUrl);
  }

}
