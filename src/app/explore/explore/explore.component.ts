import { Component, OnInit } from '@angular/core';
import { ethers } from 'ethers';
import { HttpService } from 'src/app/services/http.service';
import { SignerService } from 'src/app/services/signer.service';
import { NgxUiLoaderService } from 'ngx-ui-loader'
@Component({
  selector: 'app-explore',
  templateUrl: './explore.component.html',
  styleUrls: ['./explore.component.css']
})
export class ExploreComponent implements OnInit {
  address: any
  localContract: any
  data: any = []
  nftObj: any = {}
  constructor(private signerAddress: SignerService, private http: HttpService, private loader: NgxUiLoaderService) {
  }

  async onBuy(data: any) {
    try {
      this.loader.start()
      const value = await this.localContract.createMarketSale(data.tokenId, { value: ethers.utils.parseUnits(data.price.toString(), 'wei') });
      await value.wait()
      alert(`Your NFT ${data.name} has been successfully purchased.`)
      window.location.reload()
      this.loader.stop()
    } catch (err) {
      alert(err)
      this.loader.stop()
    }
  }


  async explore() {
    const allMarketItems = await this.localContract.fetchMarketItems();
    for (let i = 0; i < allMarketItems.length; i++) {
      
      if((allMarketItems[i].tokenId).toNumber() != "0"){
        
        this.nftObj.address = allMarketItems[i].seller
        this.nftObj.tokenId = (allMarketItems[i].tokenId).toNumber()
        this.nftObj.price = (allMarketItems[i].price).toNumber()
        this.nftObj.uri = await this.localContract.tokenURI(this.nftObj.tokenId);
        this.data.push(this.nftObj)
        this.nftObj = {}
      }
    }
    this.http.create(this.data, 'fetchAll').subscribe((res) => {
      this.nftObj = res
      this.loader.stop()
    })

    return this.nftObj
  }
  ngOnInit(): void {

    this.loader.start()
      const signer = this.signerAddress.getsignerAddresss()
      signer.subscribe({
        next: (address: any) => {
          this.address = address.signer
          this.localContract = address.localContract
          this.explore()
        }
      })

  }
}