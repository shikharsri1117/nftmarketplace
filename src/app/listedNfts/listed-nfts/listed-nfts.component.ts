import {  Component, OnInit } from '@angular/core';
import { HttpService } from 'src/app/services/http.service';
import { SignerService } from 'src/app/services/signer.service';
import { NgxUiLoaderService } from 'ngx-ui-loader'

@Component({
  selector: 'app-listed-nfts',
  templateUrl: './listed-nfts.component.html',
  styleUrls: ['./listed-nfts.component.css']
})

export class ListedNftsComponent implements OnInit {
  address: any
  localContract: any
  data: any = []
  nftObj: any = {}
  endAuction: any

  constructor(private signerAddress: SignerService, private http: HttpService, private loader: NgxUiLoaderService) {
  }

  async onAuction(data: any) {
    try {
      this.loader.start()
      const value = await this.localContract.start(data.tokenId)
      await value.wait()
      this.loader.stop()

      let time = (await this.localContract.endAt((data.tokenId).toString())).toNumber()
      time = (time * 1000) - Date.now()

      setTimeout(this.endAuction = async () => {
        const token = data.tokenId
        this.loader.start()
        await this.localContract.end(token).then((res: any, err: any) => {
          if (res) {
            alert("Auction Ended")
            this.loader.stop()
          } else {
            console.log(err)
            this.loader.stop()
          }
        })
        setInterval(this.endAuction,30000)
        this.loader.stop()
      }, time + 1000);
      alert(`Auction for ${data.name} has started.`)
      window.location.reload()

    } catch (err) {
      alert(err)
      this.loader.stop()
    }
  }

  async explore() {

    const allMarketItems = await this.localContract.fetchItemsListed();
    for (let i = 0; i < allMarketItems.length; i++) {
      if ((allMarketItems[i].tokenId).toNumber() != "0") {
        this.nftObj.address = allMarketItems[i].seller
        this.nftObj.uri = await this.localContract.tokenURI((allMarketItems[i].tokenId).toNumber())
        this.nftObj.price = (allMarketItems[i].price).toNumber()
        this.nftObj.tokenId = (allMarketItems[i].tokenId).toNumber()
        this.data.push(this.nftObj)
        this.nftObj = {}
      }
    }

    this.http.create(this.data, 'fetchAll').subscribe((res) => {
      this.nftObj = res
      this.loader.stop()
    })
    return this.nftObj
  }
  ngOnInit(): void {

    this.loader.start()

    const signer = this.signerAddress.getsignerAddresss()
    signer.subscribe({
      next: (address: any) => {
        this.address = address.signer
        this.localContract = address.localContract
        this.explore()

      }
    })

  }
}