import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ethers } from 'ethers';
import { HttpService } from 'src/app/services/http.service';
import { SignerService } from 'src/app/services/signer.service';
import { NgxUiLoaderService } from 'ngx-ui-loader'

@Component({
  selector: 'app-auction',
  templateUrl: './auction.component.html',
  styleUrls: ['./auction.component.css']
})
export class AuctionComponent implements OnInit {
  address: any
  localContract: any
  data: any = []
  nftObj: any = {}
  bid!: FormGroup;
  bids: any;

  constructor(private signerAddress: SignerService, private http: HttpService, private loader: NgxUiLoaderService) {

  }

  async onBidding(data: any) {

    try {
      this.loader.start()
      this.bids = await this.bid.value.bidding.toString()

      const value = await this.localContract.bid(data.tokenId, { value: ethers.utils.parseUnits(this.bids, 'wei') });
      await value.wait()
      this.loader.stop()
      let time = (await this.localContract.endAt((data.tokenId).toString())).toNumber()
      time = (time * 1000) - Date.now()
      setTimeout(async () => {
        const token = data.tokenId
        this.loader.start()
        const value = await this.localContract.withdraw(token)
        await value.wait()
        alert("Auction Lost! Ether reverted!")
        this.loader.stop()
        window.location.reload()
      }, time + 2000);
      alert(`Bid on ${data.name} of ${this.bids}wei has been made.`)
      window.location.reload()
    } catch (err) {
      console.log(err)
      this.loader.stop()
    }
  }


  async myNfts() {
    
    const allMarketItems = await this.localContract.fetchAuction();
    for (let i = 0; i < allMarketItems.length; i++) {
      this.nftObj.address = allMarketItems[i].seller
      this.nftObj.tokenId = (allMarketItems[i].tokenId).toNumber()
      this.nftObj.uri = await this.localContract.tokenURI((allMarketItems[i].tokenId).toNumber())
      this.nftObj.timestamp = (await this.localContract.endAt((allMarketItems[i].tokenId).toNumber())).toNumber()
      this.nftObj.highestBid = (await this.localContract.highestBid((allMarketItems[i].tokenId).toNumber())).toNumber()
      this.nftObj.price = (allMarketItems[i].price).toNumber()
      this.data.push(this.nftObj)
      this.nftObj = {}
    }


    this.http.create(this.data, 'fetchAll').subscribe((res) => {
      this.nftObj = res
      this.loader.stop()
    })
    return this.nftObj
  }

  ngOnInit(): void {
    
    this.bid = new FormGroup({
      bidding: new FormControl("", [Validators.required])
    })

    this.loader.start()
    const signer = this.signerAddress.getsignerAddresss()
    signer.subscribe({
      next: (address: any) => {
        this.address = address.signer
        this.localContract = address.localContract
        this.myNfts()

      }
    })
  }

}
