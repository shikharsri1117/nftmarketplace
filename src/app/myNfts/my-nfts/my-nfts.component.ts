import { AfterViewInit, Component, OnInit, } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ethers } from 'ethers';
import { HttpService } from 'src/app/services/http.service';
import { SignerService } from 'src/app/services/signer.service';
import { NgxUiLoaderService } from 'ngx-ui-loader'

@Component({
  selector: 'app-my-nfts',
  templateUrl: './my-nfts.component.html',
  styleUrls: ['./my-nfts.component.css']
})
export class MyNftsComponent implements OnInit {
  address: any
  localContract: any
  data: any = []
  nftObj: any = {}
  resell!: FormGroup;
  sellingPrice: any;
  endAuction: any;
  constructor(private signerAddress: SignerService, private http: HttpService, private loader: NgxUiLoaderService) {

  }


  resellPrice() {
    this.sellingPrice = this.resell.value.resellPrice
  }
  async onResell(data: any) {
    try {
      this.loader.start()
      const listingPrice = await this.localContract.getListingPrice()
      const value = await this.localContract.resellToken(data.tokenId, this.sellingPrice, { value: ethers.utils.parseUnits(listingPrice.toString(), 'wei') })
      await value.wait();
      this.loader.stop();
      alert(`${data.name} has been put for resale.`)
    } catch (err) {
      console.log(err)
      this.loader.stop()
    }
  }

  async onAuction(data: any) {
    try {
      this.loader.start()
      const listingPrice = await this.localContract.getListingPrice()
      const value = await this.localContract.start(data.tokenId, { value: ethers.utils.parseUnits(listingPrice.toString(), 'wei') })
      await value.wait()
      this.loader.stop()
      let time = (await this.localContract.endAt((data.tokenId).toString())).toNumber();
      time = (time * 1000) - Date.now()
      setTimeout(this.endAuction = async () => {
        const token = data.tokenId
        this.loader.start()
        await this.localContract.end(token).then((res: any, err: any) => {
          if (res) {
            alert("Auction Ended")
            this.loader.stop()
            window.location.reload()
          } else {
            console.log(err)
            this.loader.stop()
          }
        })
        setInterval(this.endAuction,30000)
        this.loader.stop()
      }, time + 1000);
      alert(`Auction for ${data.name} has started.`)
      window.location.reload()

    } catch (err) {
      alert(err)
      this.loader.stop()
    }
  }

  async myNfts() {
    const allMarketItems = await this.localContract.fetchMyNfts();
    for (let i = 0; i < allMarketItems.length; i++) {
      this.nftObj.address = allMarketItems[i].owner
      this.nftObj.tokenId = (allMarketItems[i].tokenId).toNumber()
      this.nftObj.uri = await this.localContract.tokenURI((allMarketItems[i].tokenId).toNumber())
      this.nftObj.price = (allMarketItems[i].price).toNumber()
      this.data.push(this.nftObj)

      this.nftObj = {}
    }

    this.http.create(this.data, 'fetchAll').subscribe((res) => {
      this.nftObj = res
      this.loader.stop()
    })
    return this.nftObj
  }

  ngOnInit(): void {
    this.resell = new FormGroup({
      resellPrice: new FormControl("", [Validators.required])
    })

    this.loader.start()
    const signer = this.signerAddress.getsignerAddresss()
    signer.subscribe({
      next: (address: any) => {
        this.address = address.signer
        this.localContract = address.localContract
        this.myNfts()

      }
    })
  }
}


