import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ethers } from 'ethers';
import * as FormData from 'form-data';
import { HttpService } from 'src/app/services/http.service';
import { SignerService } from 'src/app/services/signer.service';
import { NgxUiLoaderService } from 'ngx-ui-loader'

@Component({
  selector: 'app-create-nft',
  templateUrl: './create-nft.component.html',
  styleUrls: ['./create-nft.component.css']
})
export class CreateNftComponent implements OnInit {

  createNft!: FormGroup;

  address: any
  localContract: any
  uri: any
  nftData: any = {}


  constructor(private signerAddress: SignerService, private http: HttpService, private loader: NgxUiLoaderService) { }

  ngOnInit(): void {
    const signer = this.signerAddress.getsignerAddresss()
    signer.subscribe({
      next: (address: any) => {
        this.address = address.signer
        this.localContract = address.localContract
      }
    })

    this.createNft = new FormGroup({
      name: new FormControl('', [Validators.required]),
      description: new FormControl('', [Validators.required]),
      price: new FormControl('', [Validators.required]),
      file: new FormControl('', [Validators.required]),
    });
  }
  localUrl: any
  imageData: any;
  onFileSelect(event: any) {
    if (event.target.files && event.target.files[0]) {

      var reader = new FileReader();
      reader.onload = (event: any) => {
        this.localUrl = (<FileReader>event.target).result;
      }
      this.imageData = new FormData();
      this.imageData.append('image', event.target.files[0])

    }
  }

  async onSubmit() {
    this.loader.start()
    this.http.create(this.imageData, 'create').subscribe(async (res) => {
      this.uri = await res
      this.nftData.name = this.createNft.value.name
      this.nftData.description = this.createNft.value.description
      this.nftData.image = this.uri

      this.http.create(this.nftData, 'createNft').subscribe(async (res) => {
        this.uri = await res
        this.loader.stop()
        if (this.createNft.valid) {
          this.loader.start()
          const listingPrice = await this.localContract.getListingPrice()
          const value = await this.localContract.createToken(this.uri, this.createNft.value.price, { value: ethers.utils.parseUnits(listingPrice.toString(), 'wei') });
          await value.wait()
          alert("NFT Succesfully Created")
          this.loader.stop()
          window.location.reload()

        }
      })
    })



  }

  _v() {
    return this.createNft.value
  }

}
